<?php
		session_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.search-container input,.search-container button{
	position:relative;
  height:48px;
	margin:0px;
  padding:0px;
  border:none;
  font-size:20px;
}
.search-container button{
  width:48px;
  float:right;
}
body{
	padding-top:40px;
	margin:0px;
  background-color:rgba(0,0,20,1);
}
body {font-family: Calibri, sans-serif;}

.top{position:fixed;
		top:0;
		width:100%;
		z-index:3;
	}
.navbar {
	/*position:fixed;
	top:0;
	*/
	overflow: hidden;
    background-color: #000816;
    font-family: Arial, Helvetica, sans-serif;
    width:100%;
    z-index: 3;
}

.navbar a {
    float: left;
    font-size: 16px;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

.dropdown {
    float: left;
    overflow: hidden;
}

.dropdown .dropbtn {
    font-size: 16px;    
    border: none;
    outline: none;
    color: white;
    padding: 14px 16px;
    background-color: inherit;
    font-family: inherit;
    margin: 0;
}

.navbar a:hover, .dropdown:hover .dropbtn {
    background-color: gray;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width:120px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 3;
}

.dropdown-content a {
    float: none;
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content a:hover {
    background-color: #ddd;
}

.dropdown:hover .dropdown-content {
    display: block;
}
</style>
</head>
<div class="top">
<div class="navbar">
  <a href="newuserhome.php" style="font-size:40px;padding:0"><b>PRIME FLIX</b> </a>
  <div class="dropdown">
    <button class="dropbtn">Browse 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
      <a href="newatoz.php">A to Z</a>
      <a href="genre.php">Genre</a>
    </div>
  </div> 

  <?php
  if(isset($_SESSION['unam'])){
echo  "<div class=\"dropdown\" style=\"float:right\">
    <button class=\"dropbtn\">".$_SESSION['unam']. 
      " <i class=\"fa fa-caret-down\"></i>
    </button>
    <div class=\"dropdown-content\">
      <a href=\"newmylist.php\">My List</a>
      
      <a href=\"logout.php\">Logout</a>
    </div>
  </div>";
}
else{
    header("Location: login.php");
}
?> 
 <div class="search-container" style="float:right">
    <form action="search.php">
      <input type="text" placeholder="Search.." name="search">
      <button type="submit" ><i class="fa fa-search"></i></button>
    </form>
 </div>
</div>
</div>