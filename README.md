# miniproject-website
Website(mini project) created in third year of B.E. IT
A movie library website which has the basic CRUD(Create Read Update Delete) operations. This mini project was a part of our curriculum for third year engineering for Database Management Systems. Requirement was to create a database and an easy to access interface using. The database had to be created using MySQL and user interface was created As a website using php for back end. The database used is 3NF normalized. Technologies used : MySQL, php ,html, css.

This website has been made on an apache server (XAMPP).
